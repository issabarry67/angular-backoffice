import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './public/not-found/not-found.component';

const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:'full'},
  {path: '',loadChildren: () => import('./public/public.module').then(mod => mod.PublicModule)},
  {path:'contacts', loadChildren: () => import('./applications/contacts/contacts.module').then(mod => mod.ContactsModule)},
  {path:'articles', loadChildren: () => import('./applications/articles/articles.module').then(mod => mod.ArticlesModule)},
  {path:'ventes', loadChildren: () => import('./applications/ventes/ventes.module').then(mod => mod.VentesModule)},
  {path:'**', component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
