import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentesEidtComponent } from './ventes-eidt.component';

describe('VentesEidtComponent', () => {
  let component: VentesEidtComponent;
  let fixture: ComponentFixture<VentesEidtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentesEidtComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VentesEidtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
