import { Address } from "../address/address";

 

export class Contact {
    id?: number;
    name: string;
    phone: string;
    email: string;
    address: Address;

    constructor()
    {
        this.name = "";
        this.phone = "";
        this.email = "";
        this.address = new Address();
    }
}
