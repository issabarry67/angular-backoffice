import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentesAllComponent } from './ventes-all.component';

describe('VentesAllComponent', () => {
  let component: VentesAllComponent;
  let fixture: ComponentFixture<VentesAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentesAllComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VentesAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
