import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SharedRoutingModule } from './shared-routing.module';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [
    NavbarComponent,
    SideMenuComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
  ], 
  exports: [
    NavbarComponent,
    SideMenuComponent,
    FooterComponent
  ],
 
})
export class SharedModule { }
