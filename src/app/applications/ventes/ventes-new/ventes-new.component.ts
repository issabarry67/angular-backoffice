import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-ventes-new',
  templateUrl: './ventes-new.component.html',
  styleUrls: ['./ventes-new.component.css']
})
export class VentesNewComponent implements OnInit {

 
  error_messages   = {
    'contact' : [
      {type:'required', message:'Le choix d\'un client est obligatoire.'},
      {type: 'pattern', message: 'Client invalid.' },
    ],
    'article' : [
      {type:'required', message:'Le choix d\'un article est obligatoire.'},
      {type: 'pattern', message: 'Client invalid.' },
    ],
    'qt' : [
      {type:'required', message:'La quantitée est obligatoire.'},
      {type: 'pattern', message: 'Quantité invalid.' },
    ],
    'prix' : [
      {type:'required', message:'Le prix est obligatoire.'},
      {type: 'pattern', message: 'Prix invalid.' },
    ],
   
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }


  //Methodes
  newForm: FormGroup = this.fb.group({
    contact: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(100),
    ])),
    article: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(100),
    ])),
    prix: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern("^[0-9]{1,7}$"),//Limit millions
    ])),
    qt: new FormControl('',Validators.compose([
      Validators.required,
      Validators.pattern("^[0-9]{1,2}$"),//Limit millions
    ])),
  })

  onSubmit(): void
  {
    console.log(this.newForm.value);
    
  }

}
