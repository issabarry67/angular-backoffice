import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentesDetailComponent } from './ventes-detail.component';

describe('VentesDetailComponent', () => {
  let component: VentesDetailComponent;
  let fixture: ComponentFixture<VentesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentesDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VentesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
