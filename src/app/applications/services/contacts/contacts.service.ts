import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../../models/contact/contact';

//Hedaer Option
const httpOption = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
   
  })
};

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private url = "http://localhost:8080"

  constructor(private http: HttpClient) { }
  
   //Méthodes utiles
   private log(log: string){
    console.info(log)
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  
  
  getContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${this.url}/contacts`).pipe(
      tap((_) => this.log(`Recuperation des items au Complet`)),
      catchError(this.handleError('getContacts', []))
    );
  }

  getContact(id: number): Observable<Contact>{
    return this.http.get<Contact>(`${this.url}/contacts/${id}`).pipe(
      tap((_) => this.log(`Le conatact a été trouvé !`)),
      catchError(this.handleError<Contact>(`getContact id=${id}`))
    );
  }

  newContact(contact: Contact): Observable<Contact>{
    return this.http.post<Contact>(`${this.url}/users`, contact, httpOption).pipe(
      catchError(this.handleError('newContact', contact))
    );
  }

  editContact(contact: Contact): Observable<Contact>{
    return this.http.put<Contact>(`${this.url}/users/${contact.id}`, contact, httpOption).pipe(
      catchError(this.handleError('editContact', contact))
    );
  }

  patchContact(contact: Contact): Observable<Contact>{
    return this.http.patch<Contact>(`${this.url}/users/${contact.id}`, contact, httpOption).pipe(
      catchError(this.handleError('patchContact', contact))
    );
  }

  deletContact(id: number): Observable<unknown>{
    return this.http.delete<Contact>(`${this.url}/users/${id}`, httpOption).pipe(
      catchError(this.handleError('deletContact'))
    );
  }

}
