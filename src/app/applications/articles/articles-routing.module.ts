import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesAllComponent } from './articles-all/articles-all.component';
import { ArticlesDetailComponent } from './articles-detail/articles-detail.component';
import { ArticlesEditComponent } from './articles-edit/articles-edit.component';
import { ArticlesNewComponent } from './articles-new/articles-new.component';

const routes: Routes = [
  {path:'', redirectTo:'all', pathMatch:'full'},
  {path:'all', component:ArticlesAllComponent},
  {path:'new', component:ArticlesNewComponent},
  {path:'detail/:', component:ArticlesDetailComponent},
  {path:'edit/:', component:ArticlesEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
