import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { ContactsService } from './applications/services/contacts/contacts.service';
import { PublicModule } from './public/public.module';
 

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    PublicModule,
    AppRoutingModule,
    SharedModule,
    
  ],
  providers: [ContactsService],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
