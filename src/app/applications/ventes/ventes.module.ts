import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentesRoutingModule } from './ventes-routing.module';
import { VentesAllComponent } from './ventes-all/ventes-all.component';
import { VentesNewComponent } from './ventes-new/ventes-new.component';
import { VentesDetailComponent } from './ventes-detail/ventes-detail.component';
import { VentesEidtComponent } from './ventes-eidt/ventes-eidt.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    VentesAllComponent,
    VentesNewComponent,
    VentesDetailComponent,
    VentesEidtComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    VentesRoutingModule
  ],
  exports: [
    VentesAllComponent,
    VentesNewComponent,
    VentesDetailComponent,
    VentesEidtComponent
  ]
})
export class VentesModule { }
