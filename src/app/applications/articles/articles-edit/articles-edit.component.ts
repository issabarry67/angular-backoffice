import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-articles-edit',
  templateUrl: './articles-edit.component.html',
  styleUrls: ['./articles-edit.component.css']
})
export class ArticlesEditComponent implements OnInit {

  error_messages   = {
    'name' : [
      {type:'required', message:'Le nom est obligqtoire.'},
      {type: 'minlength', message: 'Nom trop court.' },
      {type: 'maxlength', message: 'Nom trop long.' },
      {type: 'pattern', message: 'Format de nom invalid.' },
    ],
    'qt' : [
      {type:'required', message:'Le nom est obligqtoire.'},
      {type: 'pattern', message: 'Quantité invalid.' },
    ],
   
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }


  //Methodes
  newForm: FormGroup = this.fb.group({
    name: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(100),
      Validators.pattern("^[a-zA-Z _-]*$")
    ])),
    qt: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern("^[0-9]{1,7}$"),//Limit millions
    ])),
  })

  onSubmit(): void
  {
    console.log(this.newForm.value);
    
  }

}
