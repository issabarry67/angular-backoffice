import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesAllComponent } from './articles-all/articles-all.component';
import { ArticlesEditComponent } from './articles-edit/articles-edit.component';
import { ArticlesDetailComponent } from './articles-detail/articles-detail.component';
import { ArticlesNewComponent } from './articles-new/articles-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ArticlesAllComponent,
         ArticlesEditComponent,
         ArticlesDetailComponent,
         ArticlesNewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ArticlesRoutingModule
  ],
  exports: [
           ArticlesAllComponent,
           ArticlesEditComponent,
           ArticlesDetailComponent,
           ArticlesNewComponent,
         
  ]
})
export class ArticlesModule { }
