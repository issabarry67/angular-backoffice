import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from '../../models/address/address';
import { Contact } from '../../models/contact/contact';
import { ContactsService } from '../../services/contacts/contacts.service';

@Component({
  selector: 'app-contacts-edit',
  templateUrl: './contacts-edit.component.html',
  styleUrls: ['./contacts-edit.component.css']
})
export class ContactsEditComponent implements OnInit {
  //Variables
  @Input() contact: Contact = new Contact()
  id:      number  = this.activatedRoute.snapshot.params['id'];
  error_messages   = {
    'name' : [
      {type:'required', message:'Le nom est obligqtoire.'},
    ],
    'email' : [
      {type:'required', message:'L\'email est obligatoir.'}
    ],
    'phone' : [
      {type:'required', message:'Le numéro de telephone est obligatoir.'}
    ],
    'street' : [
      {type:'required', message:'L\'address est obligatoir.'}
    ],
    'zipcode' : [//Code postal
      {type:'required', message:'Le code postal est obligatoir.'}
    ],
    'city' : [
      {type:'required', message:'Le nom de la ville est obligatoir.'}
    ]
  }

  //Constructeur
  constructor(
    private fb: FormBuilder,
    private contactService: ContactsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.onGetContact()
  }

  //Methodes
  onGetContact(): void{
    this.contactService.getContact(this.id).subscribe({next:(resp) =>this.contact = resp })
  }

  editForm: FormGroup = this.fb.group({
    name: new FormControl('', Validators.compose([
      Validators.required,
    ])),
    email: new FormControl('', Validators.compose([
      Validators.required,
    ])),
    phone: new FormControl('', Validators.compose([
      Validators.required,
    ])),
    street: new FormControl('', Validators.compose([
      Validators.required,
    ])),
    city: new FormControl('', Validators.compose([
      Validators.required,
    ])),
    zipcode: new FormControl('', Validators.compose([
      Validators.required,
    ])),
  })

  onSubmit():void {
    
    if(this.editForm.invalid) return;
    else {
          let data = this.editForm.value;
          let contactToAdd = new Contact();
          let addresToAdd =  new Address();

          addresToAdd.street = data.street;
          addresToAdd.city = data.city;
          addresToAdd.zipcode = data.zipcode;

          contactToAdd.id = this.id;
          contactToAdd.name = data.name;
          contactToAdd.email = data.email;
          contactToAdd.phone = data.phone;
          contactToAdd.address = addresToAdd

          this.contactService.editContact(contactToAdd).subscribe({
            next: (v) => console.log(v)
            
          })

          // On lui redirige vers detail.
          this.router.navigate(['/contacts/detail', this.id])
          
    }
   
    
  }
}
