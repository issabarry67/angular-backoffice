import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../../models/contact/contact';
import { ContactsService } from '../../services/contacts/contacts.service';

@Component({
  selector: 'app-contacts-all',
  templateUrl: './contacts-all.component.html',
  styleUrls: ['./contacts-all.component.css']
})
export class ContactsAllComponent implements OnInit {
  listContacts: Contact[] = [];

  constructor(
              private router: Router,
              private contactService: ContactsService
              ) { }

              ngOnInit(): void {
                this.onGetContacts();
             }
           
             //Méthodes
             onGetContacts(): void {
                this.contactService.getContacts().subscribe({
                next: (v) => console.log(this.listContacts = v),
              }) 
             }
          

            goDetail(contact: Contact){         
                this.router.navigate(['/contacts/detail', contact.id]);
            }

            goNew(){
              this.router.navigate(['/contacts/new'])
            }

}
