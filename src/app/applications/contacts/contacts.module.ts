import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactsService } from '../services/contacts/contacts.service';
import { ContactsAllComponent } from './contacts-all/contacts-all.component';
import { ContactsDetailComponent } from './contacts-detail/contacts-detail.component';
import { ContactsEditComponent } from './contacts-edit/contacts-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactsNewComponent } from './contacts-new/contacts-new.component';
 
 
 

@NgModule({
  declarations: [
    ContactsAllComponent,
    ContactsDetailComponent,
    ContactsEditComponent,
    ContactsNewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContactsRoutingModule
  ],
  providers: [ContactsService],
})
export class ContactsModule { }
