import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from '../../models/address/address';
import { Contact } from '../../models/contact/contact';
import { ContactsService } from '../../services/contacts/contacts.service';

@Component({
  selector: 'app-contacts-new',
  templateUrl: './contacts-new.component.html',
  styleUrls: ['./contacts-new.component.css']
})
export class ContactsNewComponent implements OnInit {

  //Variables
  id:      number  = this.activatedRoute.snapshot.params['id'];

  error_messages   = {
    'name' : [
      {type:'required', message:'Le nom est obligqtoire.'},
      {type: 'minlength', message: 'Nom trop court.' },
      {type: 'maxlength', message: 'Nom trop long.' },
    ], 
    'email' : [
      {type:'required', message:'L\'email est obligatoir.'},
      {type:'email', message:'L\'email est invalide.'},
    ],
    'phone' : [
      {type:'required', message:'Le numéro de telephone est obligatoir.'},
      {type: 'minlength', message: 'Numéro trop court.' },
      {type: 'maxlength', message: 'Numéro trop long.' },
      {type: 'pattern', message: 'Numéro invalid.' },
    ],
    'street' : [
      {type:'required', message:'L\'address est obligatoir.'},
      {type:'pattern', message:'L\'address est invalid (les caracteres speciaux ne sont pas accepter).'}
    ],
    'zipcode' : [//Code postal
    {type:'required', message:'Le code postal est obligatoir.'},
    {type:'pattern', message:'Code postal invalid.'},
    {type: 'minlength', message: 'Numéro trop court.'},
    {type: 'maxlength', message: 'Numéro trop long.'},
    ],
    'city' : [
      {type:'required', message:'Le nom de la ville est obligatoir.'},
      {type: 'minlength', message: 'Nom de la ville trop court.' },
      {type: 'maxlength', message: 'Nom del a ville trop long.' },
    ]
  }

  //Constructeur
  constructor(
    private fb: FormBuilder,
    private contactService: ContactsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

  //Methodes
  editForm: FormGroup = this.fb.group({
    name: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(100),
    ])),
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.email,
    ])),
    phone: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(13),
      Validators.pattern("^[0-9 ]*$")
    ])),
    street: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern("^[a-zA-Z0-9 ]*$")
    ])),
    city: new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(50),
    ])),
    zipcode: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(5),
      Validators.maxLength(5),
    ])),
  })

  onSubmit():void {
    
    if(this.editForm.invalid) return;
    else {
          let data = this.editForm.value;
          let contactToAdd = new Contact();
          let addresToAdd =  new Address();

          addresToAdd.street = data.street;
          addresToAdd.city = data.city;
          addresToAdd.zipcode = data.zipcode;

          contactToAdd.name = data.name;
          contactToAdd.email = data.email;
          contactToAdd.phone = data.phone;
          contactToAdd.address = addresToAdd

          this.contactService.newContact(contactToAdd).subscribe({
            next: (v) => console.log(v)
            
          })

          this.editForm.reset()
          // On lui redirige vers detail.
          // this.router.navigate(['/contacts/all'])
          
    }
   
    
  }

}
