import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsAllComponent } from './contacts-all/contacts-all.component';
import { ContactsDetailComponent } from './contacts-detail/contacts-detail.component';
import { ContactsEditComponent } from './contacts-edit/contacts-edit.component';
import { ContactsNewComponent } from './contacts-new/contacts-new.component';

const routes: Routes = [
  {path:'', redirectTo:'all', pathMatch:'full'},
  {path:'new', component:ContactsNewComponent},
  {path:'all', component:ContactsAllComponent},
  {path:'detail/:id', component:ContactsDetailComponent},
  {path:'edit/:id', component:ContactsEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule { }
