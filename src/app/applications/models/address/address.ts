export class Address {
    street: string;
    city: string;
    zipcode: number;

    constructor()
    {
        this.street = "";
        this.city ="";
        this.zipcode = 0;
    }
}
