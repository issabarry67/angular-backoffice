import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VentesAllComponent } from './ventes-all/ventes-all.component';
import { VentesDetailComponent } from './ventes-detail/ventes-detail.component';
import { VentesEidtComponent } from './ventes-eidt/ventes-eidt.component';
import { VentesNewComponent } from './ventes-new/ventes-new.component';

const routes: Routes = [
  {path:'', redirectTo:'all', pathMatch:'full'},
  {path:'all', component:VentesAllComponent},
  {path:'new', component:VentesNewComponent},
  {path:'detail/:', component:VentesDetailComponent},
  {path:'edit/:', component:VentesEidtComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentesRoutingModule { }
