import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articles-all',
  templateUrl: './articles-all.component.html',
  styleUrls: ['./articles-all.component.css']
})
export class ArticlesAllComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  goDetail(): void{
    this.router.navigate(['/articles/detail/1'])
  }

  
  goNew()
  {
    this.router.navigate(['/articles/new']);
  }


}
