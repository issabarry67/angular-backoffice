import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentesNewComponent } from './ventes-new.component';

describe('VentesNewComponent', () => {
  let component: VentesNewComponent;
  let fixture: ComponentFixture<VentesNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentesNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VentesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
