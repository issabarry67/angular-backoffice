import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articles-detail',
  templateUrl: './articles-detail.component.html',
  styleUrls: ['./articles-detail.component.css']
})
export class ArticlesDetailComponent implements OnInit {
  // id: number = this.activatedRoute.snapshot.params['id'];
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goEdit(): void {
    // this.router.navigate(['/contacts/edit', this.id])
    this.router.navigate(['/articles/edit/1']);
  }

}
