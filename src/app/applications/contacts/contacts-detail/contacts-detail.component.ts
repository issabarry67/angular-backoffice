import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../../models/contact/contact';
import { ContactsService } from '../../services/contacts/contacts.service';

@Component({
  selector: 'app-contacts-detail',
  templateUrl: './contacts-detail.component.html',
  styleUrls: ['./contacts-detail.component.css']
})
export class ContactsDetailComponent implements OnInit {
  //Varibale
  contact: Contact = new Contact();
  id: number = this.activatedRoute.snapshot.params['id'];

  constructor(
    private activatedRoute: ActivatedRoute,
    private contactService: ContactsService,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.onGetContact()
  }

  onGetContact(): void{
      this.contactService.getContact(this.id).subscribe({
        next: (resp) => this.contact = resp
      })
    }

    goEdit(): void {
      this.router.navigate(['/contacts/edit', this.id])
    }

    goDelet(): void{
      this.contactService.deletContact(this.id).subscribe({next: (v) => console.log(v),
      })
      this.router.navigate(['/contacts/all'])
    }

}
